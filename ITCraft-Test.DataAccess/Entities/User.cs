﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ITCraft_Test.DataAccess.Entities
{
    [Table("AspNetUsers")]
    public class User : IdentityUser
    {
        public string Login { get; set; }
        public virtual List<Token> Tokens { get; set; } = new List<Token> { };
    }
}
