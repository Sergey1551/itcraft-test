﻿using Dapper.Contrib.Extensions;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITCraft_Test.DataAccess.Entities
{
    public class Token
    {
        public long Id { get; set; }

        public string ClientId { get; set; }

        public string Value { get; set; }

        public DateTime CreationDate { get; set; }

        public string UserId { get; set; }

        public DateTime LastModifiedTime { get; set; }

        public DateTime ExpireTime { get; set; }


        [ForeignKey("UserId")]
        [Write(false)]
        public User User { get; set; }
    }
}
