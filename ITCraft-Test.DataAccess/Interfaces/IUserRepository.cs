﻿using ITCraft_Test.DataAccess.Entities;
using System.Threading.Tasks;

namespace ITCraft_Test.DataAccess.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> Find(string userName);
    }
}
