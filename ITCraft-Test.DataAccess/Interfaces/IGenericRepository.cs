﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITCraft_Test.DataAccess.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        Task Create(TEntity item);
        Task<IEnumerable<TEntity>> Get();
        Task Remove(TEntity item);
    }
}
 