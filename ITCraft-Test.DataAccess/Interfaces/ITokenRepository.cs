﻿using ITCraft_Test.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITCraft_Test.DataAccess.Interfaces
{
    public interface ITokenRepository : IGenericRepository<Token>
    {
        Task<IEnumerable<Token>> GetTokens(string id);
        Task<Token> Find(string ClientId, string RefreshToken);
    }
}
