﻿using ITCraft_Test.DataAccess.Entities;
using ITCraft_Test.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace ITCraft_Test.DataAccess.EntityRepositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly ApplicationContext _db;

        public UserRepository(ApplicationContext db, ApplicationContext context) : base(context)
        {
            _db = db;
        }

        public async Task<User> Find(string userName)
        {
            var user = await _db.Users.Where(p => p.UserName == userName).FirstOrDefaultAsync();

            return user;
        }
    }
}
