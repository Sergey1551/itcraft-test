﻿using ITCraft_Test.DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITCraft_Test.DataAccess.EntityRepositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        readonly ApplicationContext _context;
        readonly DbSet<TEntity> _db;

        public GenericRepository(ApplicationContext context)
        {
            _context = context;
            _db = context.Set<TEntity>();
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            var item = await _db.AsNoTracking().ToListAsync();

            return item;
            
        }

        public async Task Create(TEntity item)
        {
            await _db.AddAsync(item);
            _context.SaveChanges();
        }

        public async Task Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task Remove(TEntity item)
        {
            _db.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async Task Remove(IEnumerable<TEntity> items)
        {
            _db.RemoveRange(items);
            await _context.SaveChangesAsync();
        }
    }
}
