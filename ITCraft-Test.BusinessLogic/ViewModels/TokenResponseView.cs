﻿using System;

namespace ITCraft_Test.BusinessLogic.ViewModels
{
    public class TokenResponseView
    {
        public string Token {get; set; }

        public DateTime Expiration { get; set; }

        public string RefreshToken { get; set; }

        public string Roles { get; set; }

        public string UserName { get; set; }
    }
}
