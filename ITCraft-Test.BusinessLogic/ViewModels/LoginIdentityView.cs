﻿namespace ITCraft_Test.BusinessLogic.ViewModels
{
    public class LoginIdentityView
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
