﻿namespace ITCraft_Test.BusinessLogic.ViewModels
{
    public class RegisterIdentityView
    {
        public string Login { get; set; }

        public string UserName { get; set; }
      
        public string Password { get; set; }
    }
}
