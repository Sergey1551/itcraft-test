﻿namespace ITCraft_Test.BusinessLogic.ViewModels
{
    public class AuthenticateTokenView
    {
        public string GrantType { get; set; }

        public string ClientId { get; set; }

        public string UserName { get; set; }

        public string RefreshToken { get; set; }

        public string Password { get; set; }
    }
}
