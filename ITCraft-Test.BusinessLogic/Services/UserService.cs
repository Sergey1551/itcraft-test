﻿using ITCraft_Test.BusinessLogic.Services.Interfaces;
using ITCraft_Test.DataAccess.Entities;
using ITCraft_Test.DataAccess.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITCraft_Test.BusinessLogic.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<IEnumerable<User>> GetListOfUsers()
        {
            var users = await _userRepository.Get();

            return users;
        }
    }
}
