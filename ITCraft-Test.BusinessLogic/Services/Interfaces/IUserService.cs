﻿using ITCraft_Test.DataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ITCraft_Test.BusinessLogic.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetListOfUsers();
    }
}
