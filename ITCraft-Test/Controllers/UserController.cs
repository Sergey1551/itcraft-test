﻿using System.Threading.Tasks;
using ITCraft_Test.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ITCraft_Test.Web.Controllers
{
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        } 

        [HttpGet]
        [Route("getUsers")]
        public async Task<IActionResult> GetUsers()
        {
            return Ok(await _userService.GetListOfUsers()); 
        }
    }
}