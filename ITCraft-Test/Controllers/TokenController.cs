﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ITCraft_Test.BusinessLogic;
using ITCraft_Test.BusinessLogic.Services.Interfaces;
using ITCraft_Test.BusinessLogic.ViewModels;
using ITCraft_Test.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ITCraft_Test.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly UserManager<User> _userManager;

        private readonly ApplicationSettings _appSettings;

        private readonly ITokenService _tokenService;
        
        public TokenController(
            UserManager<User> userManager,
            IOptions<ApplicationSettings> appSettings,
            ITokenService tokenService)
        {
            _userManager = userManager;
            _appSettings = appSettings.Value;
            _tokenService = tokenService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateTokenView model)
        {
            if(model == null)
            {
                return new StatusCodeResult(500);
            }

            if(model.GrantType == "password")
            {
                return await GenerateNewToken(model);
            }

            if(model.GrantType == "refreshToken")
            {
                return await RefreshToken(model);
            }

            return new UnauthorizedResult();
         }

        private async Task<IActionResult> GenerateNewToken(AuthenticateTokenView model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                if (!await _userManager.IsEmailConfirmedAsync(user))
                {
                    ModelState.AddModelError(string.Empty, "User's email is not comfirmed");

                    return Unauthorized();
                }

                var newRefreshToken = CreateRefreshToken(_appSettings.ClientId, user.Id);

                var oldRefreshTokens = await _tokenService.OldRefreshTokens(user.Id);

                if (oldRefreshTokens != null)
                {
                    foreach (var oldToken in oldRefreshTokens)
                    {
                        await _tokenService.RemoveOldToken(oldToken);
                    }
                }

                await _tokenService.AddNewTokenAsync(newRefreshToken);

                var accessToken = await CreateAccessToken(user, newRefreshToken.Value);

                return Ok(new { authToken = accessToken });
            }

            ModelState.AddModelError("", "UserName/Password was not Found");

            return Unauthorized();
        }

        private async Task<TokenResponseView> CreateAccessToken(User user, string refreshToken)
        {
            double tokenExpireTime = Convert.ToDouble(_appSettings.ExpireTime);

            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appSettings.Secret));

            var roles = await _userManager.GetRolesAsync(user);

            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Role, roles.FirstOrDefault()),
                    new Claim("LoggedOn", DateTime.UtcNow.ToString()),
                }),
                Issuer = _appSettings.Site,
                Audience = _appSettings.Audience,
                Expires = DateTime.UtcNow.AddMinutes(tokenExpireTime),
                SigningCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature)
            };

            var newToken = tokenHandler.CreateToken(tokenDescriptor);

            var encodedToken = tokenHandler.WriteToken(newToken);

            return new TokenResponseView()
            {
                Token = encodedToken,
                Expiration = newToken.ValidTo,
                RefreshToken = refreshToken,
                Roles = roles.FirstOrDefault(),
                UserName = user.UserName
            };
        }
        
        private Token CreateRefreshToken(string clientId, string userId)
        {
            return new Token()
            {
                ClientId = clientId,
                UserId = userId,
                Value = Guid.NewGuid().ToString("N"),
                CreationDate = DateTime.UtcNow,
                ExpireTime = DateTime.UtcNow.AddMinutes(30),
                LastModifiedTime = DateTime.UtcNow                
            };           
        }

        private async Task<IActionResult> RefreshToken(AuthenticateTokenView model)
        {
            try
            {
                var refreshToken = await _tokenService.FindToken(model.ClientId, model.RefreshToken);
                if (refreshToken == null)
                {
                    return new UnauthorizedResult();
                }

                if (refreshToken.ExpireTime < DateTime.UtcNow)
                {
                    return new UnauthorizedResult();
                }

                var user = await _userManager.FindByIdAsync(refreshToken.UserId);

                if (user == null)
                {
                    return new UnauthorizedResult();
                }

                var newRefreshToken = CreateRefreshToken(refreshToken.ClientId, refreshToken.UserId);

                await _tokenService.RemoveOldToken(refreshToken);
                await _tokenService.AddNewTokenAsync(newRefreshToken);

                var response = await CreateAccessToken(user, newRefreshToken.Value);

                return Ok(new { authToken = response });
            }

            catch (Exception)
            {
                return new UnauthorizedResult();
            }
        }
    }
} 