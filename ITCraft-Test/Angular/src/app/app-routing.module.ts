import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Identity/login/login.component';
import { RegisterComponent } from './Identity/register/register.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path:'**', redirectTo: 'register'},
    { path: 'login', component: LoginComponent},
    { path: 'register', component: RegisterComponent },
  ])],
  exports: [RouterModule]
})

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
