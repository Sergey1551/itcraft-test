import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})

 export class AccountService {

    constructor(private http: HttpClient, private router: Router) { }

    private loginStatusItem = 'loginStatus';
    private userNameItem = 'userName';
    private userRoleItem = 'userRole';
    private jwtItem = 'jwt';
    private expirationItem = 'expiration';
    private refreshTokenItem = 'refreshToken';

    private tokenUrl = environment.baseUrl + "/token/";
    private identityUrl = environment.baseUrl + "identity/";

    private loginStatus = new BehaviorSubject<boolean>(this.checkLoginStatus());
    private userName    = new BehaviorSubject<string>(localStorage.getItem('userName'));
    private userRole    = new BehaviorSubject<string>(localStorage.getItem('userRole'));

    register(username: string, password: string, email : string ) 
    {
        return this.http.post<any>(this.identityUrl + "register", {username, password, email});
    }

    getNewRefreshToken() : Observable<any> 
    {
        let userName = localStorage.getItem('userName');
        let refreshToken = localStorage.getItem('refreshToken');
        const grantType = "refreshToken";

        return this.http.post<any>(this.tokenUrl + "authenticatete", {userName, refreshToken,  grantType}).pipe(
            map(result => {
                if(result && result.authToken.token) 
                {
                    this.loginStatus.next(true);
                    localStorage.setItem(this.loginStatusItem, '1');
                    localStorage.setItem(this.jwtItem, result.authToken.token);
                    localStorage.setItem(this.userNameItem, result.authToken.userName);
                    localStorage.setItem(this.expirationItem, result.authToken.expiration);
                    localStorage.setItem(this.userRoleItem, result.authToken.roles);
                    localStorage.setItem(this.refreshTokenItem, result.authToken.refreshToken);                    
                }

                return <any>result;            
            }) 
        );
    }

    login(username: string, password: string) 
    {
        const grantType = "password";
        return this.http.post<any>(this.tokenUrl + "authenticate", {username, password, grantType}).pipe(
            map(result => {
                if(result && result.authToken.token) 
                { 
                    this.loginStatus.next(true);
                    localStorage.setItem(this.loginStatusItem, '1');
                    localStorage.setItem(this.jwtItem, result.authToken.token);
                    localStorage.setItem(this.userNameItem, result.authToken.userName);
                    localStorage.setItem(this.expirationItem, result.authToken.expiration);
                    localStorage.setItem(this.userRoleItem, result.authToken.roles);
                    localStorage.setItem(this.refreshTokenItem, result.authToken.refreshToken);
                    this.userName.next(localStorage.getItem(this.userNameItem));
                    this.userRole.next(localStorage.getItem(this.userRoleItem));
                }

                 return result;
            })              
        );
    }

    logout() 
    {
        this.loginStatus.next(false);
        localStorage.removeItem(this.jwtItem);
        localStorage.removeItem(this.userNameItem);
        localStorage.removeItem(this.userRoleItem);
        localStorage.removeItem(this.expirationItem);
        localStorage.removeItem(this.refreshTokenItem);
        localStorage.setItem(this.loginStatusItem, '0');
        this.router.navigate(['/login']);
        console.log("Logged Out Successfully");
    }

    checkLoginStatus() : boolean 
    {        
        var loginCookie = localStorage.getItem(this.loginStatusItem);

        if(loginCookie == "0")
        {
            return false;
        }
      
        if(loginCookie == "1" && localStorage.getItem(this.jwtItem) != null || localStorage.getItem(this.jwtItem) != undefined)
        {
            return true;
        }

        return false;
    }

    get isLoggedIn() 
    {
        return this.loginStatus;
    }

    get currentUserName() 
    {
        return this.userName;
    }

   get currentUserRole() 
    {
        return this.userRole;
    }
}
